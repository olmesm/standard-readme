# Standard Readme

<!-- Insert any badges below -->

[![PRs Welcome](https://img.shields.io/badge/PRs-welcome-brightgreen.svg?style=flat-square)](http://makeapullrequest.com)

---

A basic intro - what the hell this all does - to your project goes here. 1-2 lines ideally.

## Requires

- A simple list of project requirements for development.
- Engine version managers are ideal eg [nvm].
- List any additional project dependencies.
- Look into the use of [git submodules].
- [Recommended extensions].
- Use [Markdown Reference style link] to link to their respective websites and documentation.

## Development

Any quick final notes for the developer.

```sh
# Make use of comments to explain what the command may do
#   indent follow-up comments (2 spaces)
#   Ideally we would set the language engine as the first step.
#   make use of engine manager tools for version specification eg: https://github.com/creationix/nvm#nvmrc
nvm use

# Install dependencies
npm i

# To develop
npm start
```

If additional steps in other languages are required, use [language specific code blocks].

```js
  function isSyntaxHighlightingCool(weUseIt) {
    if (weUseIt) {
      return 'then yes';
    }

    return 'otherwise it\'ll be bland and more difficult to read';
  }
```

## Additional Project Notes/Guidelines

- [Keep all additional documentaion in a `./DOCUMENTATION` or `./_documentation` directory](./_documentation)
- [Otherwise list resources that either set the standard or are heavily implemented](https://github.com/ryanmcdermott/clean-code-javascript)
- [Or even the frameworks documentation]

## Production Steps or Deployment notes

If it is the responsibility of the development team to maintain the production deployment, then that could be listed in a separate document in a documentation directory - see above. If not then this should either belong in the terraform repo, confluence, or whatever the business is agreed upon for developer/sysops owned documentation.

- How is it deployed?
- How do we define the app's environment?
- Is it a [12 factor app]?
- Where is it hosted?

## Final Notes

Assume that the developer has basic knowledge of git, the language, and following standards. Remember who this document is for.

- Automation is Authority (use a linter).
- Don't over explain.
- Don't use specific details that need to be maintained.
- If starting a project, create a `./_documentation/_template.md` for additional docs.
- Reference over repetition.
  > Rather reference specific IP addresses, API URL's, or information that would need to be changed in multiple places.
- Understand the structure or layout of the docs and maintain it.
- Unless on purpose, try keep things in alphabetical order.
- Make use of a [.editorconfig file].

## Resources

Lastly let's link to any additional resources that are handy, but not vital to the project.

- [Frontend Checklist]
- [JavaScript Clean Code Guidelines]
- [Markdown Cheatsheet]
- [Project Guidelines]

<!-- References -->

[.editorconfig file]: https://editorconfig.org/
[12 factor apps]: https://12factor.net/
[Frontend Checklist]: https://github.com/thedaviddias/Front-End-Checklist
[git submodules]: https://git-scm.com/book/en/v2/Git-Tools-Submodules
[JavaScript Clean Code Guidelines]: https://github.com/ryanmcdermott/clean-code-javascript
[language specific code blocks]: https://help.github.com/articles/creating-and-highlighting-code-blocks/
[Markdown Cheatsheet]: https://github.com/adam-p/markdown-here/wiki/Markdown-Cheatsheet
[Markdown Reference style link]: https://github.com/adam-p/markdown-here/wiki/Markdown-Cheatsheet#links
[nvm]: https://github.com/creationix/nvm
[Or even the frameworks documentation]: https://raygun.com/blog/popular-javascript-frameworks/
[Project Guidelines]: https://github.com/elsewhencode/project-guidelines
[Recommended extensions]: https://code.visualstudio.com/docs/editor/extension-gallery#_recommended-extensions